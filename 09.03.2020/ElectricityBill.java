import java.util.Scanner;

public class ElectricityBill {
public static void main(String[] args) {
	Scanner Scan = new Scanner (System.in);
	System.out.print("Enter your unit :");
	int unit = Scan.nextInt();
	
	if(unit>=0) {
		if(unit<=30) {
			int cost = (unit*3);
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		} 
		else if((unit>= 31) && (unit<=60)) {
			int cost = (30*3)+(unit-30)*5;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		}
		else if((unit>= 61) && (unit<=90)) {
			int cost = (30*3)+(30*5)+(unit-60)*10;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
	}
		else if((unit>= 91) && (unit<=120)) {
			int cost = (30*3)+(30*5)+(30*10)+(unit-90)*20;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		}
		else if((unit>= 121) && (unit<=180)) {
			int cost = (30*3)+(30*5)+(30*10)+(30*20)+(unit-120)*30;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		}
		else if((unit>= 181) && (unit<=210)) {
			int cost = (30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(unit-180)*35;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		}
		else if((unit>= 211) && (unit<=300)) {
			int cost = (30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(90*40)+(unit-300)*50;
			System.out.println("TOTAL COST :" +cost+ "PER UNIT");
		}
		else {
			System.out.println("Error");
		}
		}
}
}
