// Find the sum of natural numbers from 1 to 10
public class NaturalNumber{
	public static void main (String[] args){
		
		int sum = 0;
		
			for (int i = 1; i <=10; ++i){
				sum += i;   // sum = sum + int
			}
			
			System.out.println("sum = " + sum);
	}
}