
public class FactorialFinder {

	 public int performFactorial(int fact) {
		 
		 int factValue = 1;
		 for (int i =1; i <= fact; i++) {
			 
			 factValue = factValue*i;
		 }
		  
		 return factValue;
	 }
}
