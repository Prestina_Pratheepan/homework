import java.util.Scanner;

public class FactorialDemo {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter a number");
		int fact = scan.nextInt();
		
		FactorialFinder factFinder = new FactorialFinder();
		int factValue = factFinder.performFactorial(fact);
		
		System.out.println("Factorial of " + fact + " is = " + factValue);
		System.out.println(fact + "! = " + factValue);}
}