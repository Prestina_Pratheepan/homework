package prog.bcas.bank;

public class BankDemo {
	
	public static void main(String[] args) {
		
		BankAccount accPrestina = new BankAccount();
		BankAccount accPratheepan = new BankAccount();
		BankAccount accRamila = new BankAccount();
		
		accPrestina.openAccount("Colombo", "Saving Account", "10898907", "P.Prestina", 10000);
		accPratheepan.openAccount("Kandy", "Current Account", "20026570", "S.Pratheepan", 500000);
		accRamila.openAccount("Jaffna", "Saving Account", "20198764", "P.Ramila", 20000);
		
		accPrestina.withdrawal(2000);
		accPratheepan.deposit(2500);
		accRamila.deposit(12000);
		
		accPrestina.getAccountDetails("10898907");
		accPratheepan.getAccountDetails("20026570");
		accRamila.getAccountDetails("20198764");
		
		System.out.println(accPrestina);
		System.out.println(accPratheepan);
		System.out.println(accRamila);

	}
}




